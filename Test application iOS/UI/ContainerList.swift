//
//  ContainerList.swift
//  cloud-storage-ios-swift
//
//  Created by Wouter van de Velde on 08/10/2018.
//  Copyright © 2018 RizkySly Production. All rights reserved.
//

import UIKit

class ContainerList: UITableViewController, UIPopoverPresentationControllerDelegate {
    
    let data = StorageContainers.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "+", style: .plain, target: self, action: #selector(addContainer(sender:)))
        
        NotificationCenter.default.addObserver(self, selector: #selector(containersChanged(notification:)), name: Notification.Name("CloudStorageContainerChangedUuid"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(containersChanged(notification:)), name: Notification.Name("CloudStorageContainerDeleted"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        Statistics.addPath(path: "/list", info: "")
        
        tableView.reloadData()
    }
    
    @objc func containersChanged(notification: Notification) {
        tableView.reloadData()
    }
    
    @objc private func addContainer(sender: UIBarButtonItem){
        
        let containerAdd = ContainerAdd()
        containerAdd.modalPresentationStyle = .popover
        containerAdd.popoverPresentationController?.barButtonItem = sender
        containerAdd.popoverPresentationController?.delegate = self
        
        present(containerAdd, animated: true, completion: nil)
    }

    // MARK: TableView delegates and datasources
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return StorageContainers.shared.list.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let item = data.list[indexPath.row]
        
        let cell: UITableViewCell = {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell") else {
                let cell = UITableViewCell(style: .subtitle , reuseIdentifier: "UITableViewCell")
                cell.accessoryType = .disclosureIndicator
                return cell
            }
            return cell
        }()
        
        cell.textLabel?.text = "\(item.name)"

        if item.local {
            cell.detailTextLabel?.text = "Local"
        }
        else{
            cell.detailTextLabel?.text = "Linked"
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let containerEdit = ContainerEdit();
        containerEdit.uuid = data.list[indexPath.row].uuid
        
        navigationController?.pushViewController(containerEdit, animated: true)
    }
    
    // MARK: Popover delegates
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
        tableView.reloadData()
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle
    {
        return .none
    }
}

//
//  ContainerEdit.swift
//  cloud-storage-ios-swift
//
//  Created by Wouter van de Velde on 08/10/2018.
//  Copyright © 2018 RizkySly Production. All rights reserved.
//

import UIKit

class ContainerEdit: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    let data = StorageContainers.shared

    var uuid = ""
    var container = StorageContainers.Container()

    let sv = UIScrollView()
    let field = UITextField()
    let qrimage = UIImageView()
    let options = UITableView()
    
    //When local: Link devices, Delete
    //When online: Link devices, Reset link, Delete
    let texts = [
        ["local": "local", "alias": "link_other_device", "title": "Link other device", "description": "The container is put online so other devices can sync the content"],
        ["local": "online", "alias": "reset_link", "title": "Reset", "description": "The container is reverted to local and deleted from other devices"],
        ["local": "both", "alias": "delete_container", "title": "Delete container", "description": "Your container is perminently deleted from all devices if online"]
    ]
    var textsSelected = [[:]]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        edgesForExtendedLayout = []
        
        sv.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(sv)
        
        setUuid(uuid)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        Statistics.addPath(path: "/edit", info: "")
        
        // Write a test file
        let txtdata = "[\"This is my file\"".data(using: .utf8)
        StorageFiles.shared.addFile(container: uuid, name: "\(NSUUID().uuidString.lowercased()).json", data: txtdata!)
    }
    
    private func setUuid(_ newUuid: String){
        uuid = newUuid

        if let index = data.list.index(where: { $0.uuid == uuid }) {
            container = data.list[index]
            buildScreen()
            options.reloadData()
        }
    }
    
    private func buildScreen(){
        
        sv.subviews.forEach({ $0.removeFromSuperview() })
        
        if container.local {
            textsSelected = texts.filter{$0["local"] != "online"}
        }
        else{
            textsSelected = texts.filter{$0["local"] != "local"}
        }
        
        field.frame = CGRect(x: 16, y: 16, width: view.frame.size.width - 32, height: 40)
        field.text = container.name
        field.delegate = self
        field.layer.borderColor = UIColor.darkGray.cgColor
        field.layer.borderWidth = 1.0
        sv.addSubview(field)

        var y = CGFloat(70.0)
        
        if !container.local {
            qrimage.frame = CGRect(x: 0, y: y, width: view.frame.size.width, height: 200)
            qrimage.contentMode = .scaleAspectFit
            qrimage.image = data.qrContainer(uuid: uuid, width: 200)
            sv.addSubview(qrimage)
            
            y += 210
        }
        
        options.frame = CGRect(x: 0, y: y, width: view.frame.size.width, height: 56.0 * CGFloat(textsSelected.count))
        options.delegate = self
        options.dataSource = self
        options.isScrollEnabled = false
        sv.addSubview(options)
        
        sv.contentSize = CGSize(width: view.frame.size.width, height: y + options.frame.size.height)
    }
    
    // MARK: TableView delegates and datasources
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return textsSelected.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = textsSelected[indexPath.row]
        
        let cell: UITableViewCell = {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell") else {
                let cell = UITableViewCell(style: .subtitle , reuseIdentifier: "UITableViewCell")
                cell.accessoryType = .disclosureIndicator
                return cell
            }
            return cell
        }()
        
        cell.textLabel?.text = item["title"] as? String
        cell.detailTextLabel?.text = item["description"] as? String
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        options.deselectRow(at: indexPath, animated: true)
        
        let item = textsSelected[indexPath.row]
        let alias = item["alias"] as? String
        let title = item["title"] as? String
        let description = item["description"] as? String

        let alert = UIAlertController(title: title, message: description, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
        }))
        alert.addAction(UIAlertAction(title: "OK", style: .destructive, handler: { _ in
            switch(alias){
            case "link_other_device":
                Statistics.addAction(action: "link", info: "")
                self.data.shareContainer(uuid: self.uuid)
                self.setUuid(self.uuid)
            case "reset_link":
                Statistics.addAction(action: "reset", info: "")
                let newUuid = self.data.resetContainer(uuid: self.uuid)
                self.setUuid(newUuid)
            case "delete_container":
                Statistics.addAction(action: "delete", info: "")
                self.data.deleteContainer(uuid: self.uuid)
                self.navigationController?.popViewController(animated: true)
            default:
                break
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: TextField delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == field,
            let fieldText = textField.text {
            data.renameContainer(uuid: uuid, name: fieldText)
            
            textField.resignFirstResponder()
            return false
        }
        return true
    }
}

//
//  ContainerAdd.swift
//  cloud-storage-ios-swift
//
//  Created by Wouter van de Velde on 08/10/2018.
//  Copyright © 2018 RizkySly Production. All rights reserved.
//

import UIKit
import AVFoundation

class ContainerAdd: UIViewController, AVCaptureMetadataOutputObjectsDelegate {

    let field = UITextField()
    let scanbutton = UIButton()
    let addbutton = UIButton()
    let session = AVCaptureSession()

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        
        preferredContentSize = CGSize(width: 300, height: 105)
        popoverPresentationController?.backgroundColor = view.backgroundColor
        
        field.frame = CGRect(x: 10, y: 10, width: 280, height: 40)
        field.placeholder = "List name"
        field.layer.borderColor = UIColor.darkGray.cgColor
        field.layer.borderWidth = 1.0
        field.becomeFirstResponder()
        view.addSubview(field)

        scanbutton.frame = CGRect(x: 10, y: 55, width: 139.5, height: 40)
        scanbutton.setTitle("Scan", for: .normal)
        scanbutton.addTarget(self, action: #selector(scanContainer), for: .touchUpInside)
        scanbutton.backgroundColor = .blue
        view.addSubview(scanbutton)

        addbutton.frame = CGRect(x: 150.5, y: 55, width: 139.5, height: 40)
        addbutton.setTitle("Add", for: .normal)
        addbutton.addTarget(self, action: #selector(addContainer), for: .touchUpInside)
        addbutton.backgroundColor = .blue
        view.addSubview(addbutton)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        Statistics.addPath(path: "/add", info: "")
    }
    
    @objc private func addContainer(){
        if let fieldText = field.text {
            Statistics.addAction(action: "add", info: "")

            StorageContainers.shared.addContainer(name: fieldText)
            dismiss(animated: true, completion: nil)
            
            if let popvc = popoverPresentationController {
                popoverPresentationController?.delegate?.popoverPresentationControllerDidDismissPopover?(popvc)
            }
        }
    }

    @objc private func scanContainer(){
        
        if AVCaptureDevice.authorizationStatus(for: .video) == .authorized {
            scan()
        }
        else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                DispatchQueue.main.async {
                    if granted {
                        self.scan()
                    }
                    else {
                        let alert = UIAlertController(title: "Camera needed", message: "To scan the qr code, access to the camera is necessary. Please update your settings.", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
                        }))
                        alert.addAction(UIAlertAction(title: "Settings", style: .destructive, handler: { _ in
                            
                            if let url = URL(string:UIApplication.openSettingsURLString),
                                UIApplication.shared.canOpenURL(url) {
                                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                            }
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            })
        }
    }
    
    private func scan(){
        Statistics.addAction(action: "scan", info: "")

        preferredContentSize = CGSize(width: 300, height: 300)

        //With the help of https://stackoverflow.com/a/26788727
        if let device = AVCaptureDevice.default(for: AVMediaType.video),
            let input = try? AVCaptureDeviceInput(device: device) as AVCaptureDeviceInput {
            session.addInput(input)
            
            let output = AVCaptureMetadataOutput()
            output.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            session.addOutput(output)
            output.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
            
            let previewLayer = AVCaptureVideoPreviewLayer(session: session)
            let bounds = self.view.layer.bounds
            previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
            previewLayer.bounds = bounds
            previewLayer.position = CGPoint(x: bounds.midX, y: bounds.midY)
            
            self.view.layer.addSublayer(previewLayer)
            session.startRunning()
        }
    }
    
    // MARK: AVCapture delegates
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        for item in metadataObjects {
            if let metadataObject = item as? AVMetadataMachineReadableCodeObject,
                metadataObject.type == AVMetadataObject.ObjectType.qr,
                let jsonString = metadataObject.stringValue,
                let fieldText = field.text {
                
                if !StorageContainers.shared.addContainer(json: jsonString, name: fieldText) {
                    let alert = UIAlertController(title: "Container", message: "Unrecognized qr code", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { _ in
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
                dismiss(animated: true, completion: nil)
                session.stopRunning()

                if let popvc = popoverPresentationController {
                    popoverPresentationController?.delegate?.popoverPresentationControllerDidDismissPopover?(popvc)
                }
                break
            }
        }
    }
}

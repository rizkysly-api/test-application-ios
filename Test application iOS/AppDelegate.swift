//
//  AppDelegate.swift
//  cloud-storage-ios-swift
//
//  Created by Wouter van de Velde on 08/10/2018.
//  Copyright © 2018 RizkySly Production. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let tabbarcontroller = UITabBarController()

    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {

        RizkySlyAPI.shared.start()

        return true
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        //This is the test application to test the varius viewcontrollers.
        //Typically you should include the ContainerList() controller in your own application.
        //It needs to be added to a navigation controller!

        tabbarcontroller.viewControllers = [
            UINavigationController(rootViewController: ContainerList())
        ]
        
        window!.rootViewController = tabbarcontroller
        window!.makeKeyAndVisible()
        
        return true
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Notifications.store(tokenData: deviceToken)
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        Notifications.received(userInfo: userInfo)
        completionHandler(.newData)
    }
}

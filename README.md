# Test application iOS

This swift application is used to test the app and server API functionality.

See these repositories for more information:
* https://gitlab.com/rizkysly-api/ios-api
* https://gitlab.com/rizkysly-api/notifications-api
* https://gitlab.com/rizkysly-api/statistics-api
* https://gitlab.com/rizkysly-api/storage-api